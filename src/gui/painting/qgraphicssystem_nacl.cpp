/****************************************************************************
**
** Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the plugins of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "qgraphicssystem_nacl.h"
#include "qwindowsurface_nacl.h"
#include <QtGui/private/qpixmap_raster_p.h>

#include <nacl/nacl_av.h>
#include <stdio.h>

QT_BEGIN_NAMESPACE

QNaClGraphicsSystem::QNaClGraphicsSystem()
{
    perror("QNaClGraphicsSystem::QNaClGraphicsSystem()");
    mPrimaryScreen = new QNaClGraphicsSystemScreen();

    mPrimaryScreen->mGeometry = QRect(0, 0, 640, 480);
    mPrimaryScreen->mPhysicalSize = QSize(640 / 4, 480 / 4);
    mScreens.append(mPrimaryScreen);


    int error;
    error = nacl_multimedia_init(NACL_SUBSYSTEM_VIDEO);
    if (error == -1)
        perror("FAILED: QEventDispatcherNaCl::startingUp : nacl_multimedia_init(NACL_SUBSYSTEM_VIDEO);");

    // Get the screen size from the browser if the process is embedded.
    int isEmbedded;
    error = nacl_multimedia_is_embedded(&isEmbedded);
    if (error) {
        perror("QNaClGraphicsSystem::QNaClGraphicsSystem(): nacl_multimedia_is_embedded failed.");
    } else if (isEmbedded) {
        int width;
        int height;
        error = nacl_multimedia_get_embed_size(&width, &height);
        if (error == 0) {
            mPrimaryScreen->mGeometry.setSize(QSize(width, height));
        } else {
            perror("QNaClGraphicsSystem::QNaClGraphicsSystem(): nacl_multimedia_get_embed_size failed.");
        }
    }

    const QSize finalScreenSize = mPrimaryScreen->mGeometry.size();
    error = nacl_video_init(finalScreenSize.width(), finalScreenSize.height());
    if (error)
         perror("QNaClGraphicsSystem::QNaClGraphicsSystem(): nacl_video_init failed.");
}

QNaClGraphicsSystemScreen::~QNaClGraphicsSystemScreen()
{
    perror("QNaClGraphicsSystem::~QNaClGraphicsSystem()");
    nacl_video_shutdown();

    int error;
    error = nacl_multimedia_shutdown();
    if (error == -1)
        perror("FAILED: QEventDispatcherNaCl::closingDown : nacl_multimedia_shutdown()");
}

QPixmapData *QNaClGraphicsSystem::createPixmapData(QPixmapData::PixelType type) const
{
    return new QRasterPixmapData(type);
}

QWindowSurface *QNaClGraphicsSystem::createWindowSurface(QWidget *widget) const
{
    return new QNaClWindowSurface(mPrimaryScreen, widget);
}

QT_END_NAMESPACE
