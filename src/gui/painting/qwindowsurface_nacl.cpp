/****************************************************************************
**
** Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the QtOpenVG module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "qwindowsurface_nacl.h"
#include "qgraphicssystem_nacl.h"
#include <QtCore/qdebug.h>
#include "qpainter.h"
#include <private/qapplication_p.h>
#include <nacl/nacl_av.h>

QT_BEGIN_NAMESPACE

QNaClWindowSurface::QNaClWindowSurface
        (QNaClGraphicsSystemScreen *screen, QWidget *window)
    : QWindowSurface(window),
      mScreen(screen),
      mImage(screen->geometry().size(), QImage::Format_ARGB32_Premultiplied)
{
//    qDebug() << "QNaClWindowSurface::QNaClWindowSurface:" << (long)this;
    mImage.fill(QColor(140, 140, 140, 255).rgba());
}

QNaClWindowSurface::~QNaClWindowSurface()
{
}

QPaintDevice *QNaClWindowSurface::paintDevice()
{
//    qDebug() << "QNaClWindowSurface::paintDevice";
    return &mImage;
}

void QNaClWindowSurface::flush(QWidget *widget, const QRegion &region, const QPoint &offset)
{
    Q_UNUSED(widget);
    Q_UNUSED(region);
    Q_UNUSED(offset);


    //fprintf(stderr, "QNaClWindowSurface::flush %d %d", mImage.width(), mImage.height());

    nacl_video_update(mImage.scanLine(0));

    //qDebug() << "QNaClWindowSurface::flush()";
}

void QNaClWindowSurface::setGeometry(const QRect &rect)
{
    //fprintf(stderr, "QNaClWindowSurface::setGeometry %d %d", rect.width(), rect.height());
    //qDebug() << "QNaClWindowSurface::setGeometry:" << (long)this << rect;


    if (mImage.size().isValid()) {// use first set size only.
        QApplicationPrivate::handleGeometryChange(this->window(), QRect(QPoint(), mImage.size()));
        return;
    }

    QWindowSurface::setGeometry(rect);
    if (mImage.size() != rect.size())
        mImage = QImage(rect.size(), mScreen->format());


}

bool QNaClWindowSurface::scroll(const QRegion &area, int dx, int dy)
{
    return QWindowSurface::scroll(area, dx, dy);
}

void QNaClWindowSurface::beginPaint(const QRegion &region)
{
    Q_UNUSED(region);
}

void QNaClWindowSurface::endPaint(const QRegion &region)
{
    Q_UNUSED(region);
}

QT_END_NAMESPACE
