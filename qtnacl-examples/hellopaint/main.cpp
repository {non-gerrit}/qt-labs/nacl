/*
 * Copyright 2008, Google Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *     * Neither the name of Google Inc. nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


// defines
// TODO: fix/cleanup when various parts of system come online
// #define HAVE_SYSCONF         // enable if sysconf() is available

#include <errno.h>
#include <math.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
// STANDALONE = running without sel_ldr and nacl runtime
#if !defined(STANDALONE)
#include <nacl/nacl_av.h>
#include <nacl/nacl_srpc.h>
#endif
#if defined(STANDALONE)
#include "native_client/common/standalone.h"
#endif
#include "native_client/common/worker.h"

#include <QtGui>

// print/debug messages
static void InfoPrintf(const char *fmt, ...) {
  va_list argptr;
  va_start (argptr, fmt);
  vfprintf (stderr, fmt, argptr);
  va_end (argptr);
  fflush(stderr);
}

static void DebugPrintf(const char *fmt, ...) {
  va_list argptr;
  fprintf (stderr, "@@@ EARTH ");

  va_start (argptr, fmt);
  vfprintf (stderr, fmt, argptr);
  va_end (argptr);
  fflush(stderr);
}


int g_window_width = 512;
int g_window_height = 512;
int g_num_frames = 3000;

int g_frame_checksum = 0; // used for nacl module testing

struct Surface {
  int width, height, pitch;
  uint32_t *pixels;
  Surface(int w, int h) {
    width = w;
    height = h;
    pitch = w;
    pixels = new uint32_t[w * h];
  }
  ~Surface() {
    delete[] pixels;
  }
};



class Demo
{
    Surface *surf_;
    float animate;
    int width_, height_;
public:
    void Render();
    bool PollEvents();
    Demo(Surface *s);
    ~Demo();
};

void Demo::Render() {
    QImage image((uchar *)surf_->pixels, width_, height_, QImage::Format_ARGB32_Premultiplied);
    image.fill(QColor(Qt::black).rgba());
    animate += 0.01;


    QPainter p(&image);
    p.fillRect(QRect(sin(animate ) * 100 ,20, 50,50), QColor(Qt::red));

    p.

    int r;
    r = nacl_video_update(surf_->pixels);
    if (-1 == r) {
      DebugPrintf("nacl_video_update() returned %d\n", errno);
    }

}


// Polls event queue
// returns false if the user tries to quit application early
bool Demo::PollEvents() {
  NaClMultimediaEvent event;
  // bare minimum event servicing
  while (0 == nacl_video_poll_event(&event)) {
    if (NACL_EVENT_QUIT == event.type)
      return false;
  }
  return true;
}


// Setups and initializes Demo data structures.
Demo::Demo(Surface *surf) {
  surf_ = surf;
  width_ = surf->width;
  height_ = surf->height;

}

Demo::~Demo() {
}

void RunDemo(Surface *surface) {
  Demo Demo(surface);
    for (int i = 0; i < g_num_frames; ++i) {
      Demo.Render();
      // TODO: do a proper checksum here which is numerically stable
      g_frame_checksum = i;
      DebugPrintf("Frame: %04d\r", i);
      if (!Demo.PollEvents())
        break;
    }

  DebugPrintf("\nDemo complete\n");
}


// Initializes a window buffer.
Surface* Initialize() {
  int r;
  int width;
  int height;
  r = nacl_multimedia_init(NACL_SUBSYSTEM_VIDEO | NACL_SUBSYSTEM_EMBED);
  if (-1 == r) {
    InfoPrintf("Multimedia system failed to initialize!  errno: %d\n", errno);
    exit(-1);
  }
  // if this call succeeds, use width & height from embedded html
  r = nacl_multimedia_get_embed_size(&width, &height);
  if (0 == r) {
    g_window_width = width;
    g_window_height = height;
  }
  r = nacl_video_init(g_window_width, g_window_height);
  if (-1 == r) {
    InfoPrintf("Video subsystem failed to initialize!  errno; %d\n", errno);
    exit(-1);
  }
  Surface *surface = new Surface(g_window_width, g_window_height);
  return surface;
}

// Frees a window buffer.
void Shutdown(Surface *surface) {
  delete surface;
}

// We do not start the demo right away when run as a browswer module
sem_t GlobalDemoSemaphore;

#if !defined(STANDALONE)
NaClSrpcError NaclModuleStartDemo(NaClSrpcChannel *channel,
                                  NaClSrpcArg** in_args,
                                  NaClSrpcArg** out_args) {
  DebugPrintf("Start called with %d\n", in_args[0]->u.ival);
  g_num_frames = in_args[0]->u.ival;
  sem_post(&GlobalDemoSemaphore);
  return NACL_SRPC_RESULT_OK;
}

NACL_SRPC_METHOD("start_demo:i:", NaclModuleStartDemo);

NaClSrpcError NaclModuleFrameChecksum(NaClSrpcChannel *channel,
                                      NaClSrpcArg** in_args,
                                      NaClSrpcArg** out_args) {
  DebugPrintf("checksum called: %d\n", g_frame_checksum);
  out_args[0]->u.ival = g_frame_checksum;
  return NACL_SRPC_RESULT_OK;
}

NACL_SRPC_METHOD("frame_checksum::i", NaclModuleFrameChecksum);
#endif

// Parses cmd line options, initializes surface, runs the demo & shuts down.
int main(int argc, char *argv[]) {
//QApplication app(argc, argv);

  // NOTE: We current cannot distinguish whether we run in the browser or not
  bool run_in_browser = false;
  sem_init(&GlobalDemoSemaphore, 0, 0);
  sem_post(&GlobalDemoSemaphore);
  Surface *surface = Initialize();

  do {
    DebugPrintf("Waiting on semaphore\n");
    sem_wait(&GlobalDemoSemaphore);
    RunDemo(surface);
  } while (run_in_browser);  // for now run only once

  return 0;
}
